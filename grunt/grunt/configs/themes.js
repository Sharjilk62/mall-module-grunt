/**
 * Copyright © Mall of Hospitality FZE. All rights reserved.
 */

'use strict';

module.exports = {
    en_US: {
        area: 'frontend',
        name: 'Mall/theme-frontend-default',
        locale: 'en_US',
        files: [
            'css/styles-m',
            'css/styles-l',
            'css/email',
            'css/email-inline',
            'css/print'
        ],
        dsl: 'less'
    }
};
